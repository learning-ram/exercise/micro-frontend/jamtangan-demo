import React from "react";
import { BrowserRouter } from "react-router-dom";
import PdpApp from "./components/PdpApp";
import Header from "./components/Header";

export default () => {
  return (
    <BrowserRouter>
      <Header />
      <PdpApp />
    </BrowserRouter>
  );
};
