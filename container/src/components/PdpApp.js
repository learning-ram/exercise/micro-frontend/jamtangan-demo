import { mount as pdpMount } from "pdp/PdpApp";
import React, { useRef, useEffect } from "react";

export default () => {
  const ref = useRef(null);

  useEffect(() => {
    pdpMount(ref.current);
  });

  return <div ref={ref} />;
};
