const { merge } = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const commonConfig = require("./webpack.common");
// const pkgJson = require("../package.json");

const devConfig = {
  mode: "development",
  devServer: {
    port: 8081,
    // historyApiFallback: {
    //   index: "/index.html",
    // },
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "pdp",
      filename: "remoteEntry.js",
      exposes: {
        "./PdpApp": "./src/bootstrap",
      },
      // shared: pkgJson.dependencies,  // will share all of dependencies from package.json
      // shared: ["react", "react-dom"], // only share specific
    }),
    new MiniCssExtractPlugin(),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
    }),
  ],
};

module.exports = merge(commonConfig, devConfig);
