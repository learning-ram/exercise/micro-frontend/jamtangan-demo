import App from "./App.svelte";
import "./global.css";

const mount = (el) => {
  new App({
    target: el,
  });
};

if (process.env.NODE_ENV === "development") {
  const devRoot = document.querySelector("#pdp-app");

  if (devRoot) {
    mount(devRoot);
  }
}

export { mount };
